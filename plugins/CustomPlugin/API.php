<?php
/**
 * Piwik - free/libre analytics platform
 *
 * @link http://piwik.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Piwik\Plugins\CustomPlugin;

use Piwik\DataTable;
use Piwik\DataTable\Row;
use Piwik\Db;
use Piwik\Log;

/**
 * API for plugin CustomPlugin
 *
 * @method static \Piwik\Plugins\CustomPlugin\API getInstance()
 */
class API extends \Piwik\Plugin\API
{
    /**
     * Method returning a data table in order to visualize data in the dashboard (just for testing purpose).
     * @param int    $idSite
     * @param string $period
     * @param string $date
     * @param bool|string $segment
     * @return DataTable
     */
    // public function getChangingvalues($idSite, $period, $date, $segment = false)
    // {
    //     $table = new DataTable();

    //     $numberOfRows = 100;

    //     // Generating random data (not stored in db)

    //     for ($i=0; $i<100; $i++) {
    //         $random_label = 'Thing_' . rand(0,400);
    //         $random_visits = rand(200,1000);
    //         $random_hits = rand(200,1000);
    //         if ($i <= 30) {
    //             $aDate = '2017-10-29';
    //         } else if ($i > 30 && $i <= 60) {
    //             $aDate = '2017-10-30';
    //         } else if ($i > 60 && $i <= 100) {
    //             $aDate = '2017-10-31';
    //         }

    //         $table->addRowFromArray(array(Row::COLUMNS => array(
    //             'label'     => $random_label, 
    //             'nb_visits' => $random_visits,
    //             'nb_hits'   => $random_hits,
    //             'date'      => $aDate
    //         )));
    //     }

        
    //     return $table;
    // }

    /**
     * Method returning a data table in order to visualize data in the dashboard
     */
    public function getChangingvalues($idSite, $period, $date, $segment = false) {

        // $logger = StaticContainer::getContainer()->get('Psr\Log\LoggerInterface');
        
        $table = new DataTable();

        $rows = Db::fetchAll("SELECT * FROM piwik.piwik_custom_plugin"); // TODO: db name should be retrieved by a piwik method
        foreach ($rows as $row) {
            $nbRegisteredUsers = json_decode(urldecode($row['data']), true);
            // $logger->debug('-- logger test --'); // logger doesn't seem to work...
            $aDate = $row['date'];
            $table->addRowFromArray(array(Row::COLUMNS => array(
                'label'     => $aDate, 
                'nb_visits' => $nbRegisteredUsers[$aDate]['registered_users']
            )));
        }

        return $table;
    }
}
