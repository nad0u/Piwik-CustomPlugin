<?php
/**
 * Piwik - free/libre analytics platform
 *
 * @link http://piwik.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

namespace Piwik\Plugins\CustomPlugin;

use Piwik\View;

/**
 * A controller lets you for example create a page that can be added to a menu. For more information read our guide
 * http://developer.piwik.org/guides/mvc-in-piwik or have a look at the our API references for controller and view:
 * http://developer.piwik.org/api-reference/Piwik/Plugin/Controller and
 * http://developer.piwik.org/api-reference/Piwik/View
 */
class Controller extends \Piwik\Plugin\Controller
{
    /**
     * Index page which will output a list (index is called via ajax on a page)
     */
    public function index()
    {
        // Render the Twig template templates/index.twig and assign the view variable answerToLife to the view.
        // return $this->renderTemplate('index', array(
        //     'answerToLife' => 42
        // ));

        $view = new View("@CustomPlugin/index.twig");
        // Generate the report visualization to use it in the view
        $view->myReport = $this->myReport();
        return $view->render();
    }

    /**
     * Report method
     */
    public function myReport() {
        $view = \Piwik\ViewDataTable\Factory::build(
            $defaultType = 'graphEvolution', // the visualization type
            $apiAction = 'CustomPlugin.getChangingvalues',
            $controllerMethod = 'CustomPlugin.myReport'
        );
        $view->config->show_limit_control = true;
        $view->config->show_search = false;
        $view->config->show_goals = false;

        // ... do some more configuration ...

        return $view->render();
    }
}
