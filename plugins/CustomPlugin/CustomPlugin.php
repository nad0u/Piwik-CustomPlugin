<?php
/**
 * Piwik - free/libre analytics platform
 *
 * @link http://piwik.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

namespace Piwik\Plugins\CustomPlugin;

class CustomPlugin extends \Piwik\Plugin
{

    /**
     * Create a new table and add fake data at plugin installation
     * TODO: did not test this (the creation and db population is done via an update instead)
     */
    public function install()
    {
        try { 
            $sql = "CREATE TABLE " . Common::prefixTable('custom_plugin') . " ( 
                        id INT NOT NULL AUTO_INCREMENT , 
                        date DATE( 100 ) NOT NULL , 
                        data LONGTEXT NOT NULL , 
                        PRIMARY KEY ( id ) 
                    )  DEFAULT CHARSET=utf8 ";
            Db::exec($sql);
             
            $numberOfRows = 20;
            $aDate = '2017-10-';
            $data = [];
 
            // Generating random data on 20 days
             
            for ($i=0; $i<$numberOfRows; $i++) {
                $random_users = rand(0,200);
                $aDate = '2017-10-' . ($i+1);
                unset($data); // empty array
 
                $data[$aDate] = ['registered_users' => $random_users];
 
                $data_json = json_encode($data);
                         
                $sql = 'INSERT INTO custom_plugin () VALUES(' .$aDate. ', ' .$data_json. ')';
                Db::exec($sql); 
            } 
 
        } catch (Exception $e) { 
            // ignore error if table already exists (1050 code is for 'table already exists') 
            if (!Db::get()->isErrNo($e, '1050')) { 
                throw $e; 
            } 
        } 
    }

    /**
     * Remove created table for the plugin when it is uninstalled
     */
    public function uninstall()
    {
        Db::dropTables(Common::prefixTable('custom_plugin'));
    }

}
