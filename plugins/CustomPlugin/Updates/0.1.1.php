<?php
/** 
 * Piwik - free/libre analytics platform 
 * 
 * @link http://piwik.org 
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later 
 * 
 */

namespace Piwik\Plugins\CustomPlugin;

use Piwik\Updater;
use Piwik\Updates as PiwikUpdates;
use Piwik\Updater\Migration;
use Piwik\Updater\Migration\Factory as MigrationFactory;
 
/** 
 * Update for version 0.1.1. 
 */ 
class Updates_0_1_1 extends PiwikUpdates 
{ 
    /** 
     * @var MigrationFactory 
     */ 
    private $migration; 
 
    public function __construct(MigrationFactory $factory) 
    { 
        $this->migration = $factory; 
    } 
 
    /** 
     * Return database migrations to be executed in this update. 
     * 
     * Database migrations should be defined here, instead of in `doUpdate()`, since this method is used 
     * in the `core:update` command when displaying the queries an update will run. If you execute 
     * migrations directly in `doUpdate()`, they won't be displayed to the user. Migrations will be executed in the 
     * order as positioned in the returned array. 
     * 
     * @param Updater $updater 
     * @return Migration\Db[] 
     */ 
    public function getMigrations(Updater $updater) 
    { 
        // Create new table to store fake (already aggregated/processed) data
        $migration1 = $this->migration->db->createTable( 
            'custom_plugin', [ 
                'id' => 'INTEGER NOT NULL AUTO_INCREMENT', 
                'date' => 'DATE NOT NULL', 
                'data' => 'LONGTEXT NOT NULL' 
            ], 'id');
 
        return array( 
            $migration1
        ); 
    } 
 
    /** 
     * Perform the incremental version update. 
     * 
     * This method should perform all updating logic. If you define queries in the `getMigrations()` method, 
     * you must call {@link Updater::executeMigrations()} here. 
     * 
     * @param Updater $updater 
     */ 
    public function doUpdate(Updater $updater) 
    { 
        // Generating random data over 20 days

        $numberOfRows = 20; 
        $aDate = '2017-10-'; 
        $data = []; 
        $rows_data = []; 
         
        for ($i=0; $i<$numberOfRows; $i++) { 
            $random_users = rand(0,200);
            // TODO: not good, we can have dates like "2017-10-1" instead of "2017-10-01" as it is stored as a string and note as a date..
            $aDate = '2017-10-' . ($i+1); 
 
            unset($data); // empty array just in case
 
            $data[$aDate] = ['registered_users' => $random_users];
            // TODO: maybe we can forget the urlencode here
            $data_json = urlencode(json_encode($data));
            array_push($rows_data, [$aDate, $data_json]); 
        } 
 
        // First, run the migration above (i.e. create new table)
        $updater->executeMigrations(__FILE__, $this->getMigrations($updater)); 
        // Second, populate the table with the genereated fake data
        $updater->executeMigration(null, $this->migration->db->batchInsert('custom_plugin', ['date', 'data'], $rows_data)); 
    } 
} 